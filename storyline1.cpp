#include <iostream>
#include "mathfunc.h"
#include <string>
#include <ctime>
#include <cstdlib>

#ifdef _WINDOWS
#include <Windows.h>
#else
#include <unistd.h>
#define Sleep(x) usleep((x)*1000)
#endif

using namespace std;

int atk = 10;

int def = 10;

int mag = 15;

int hp = 100;


string playername; // our player name
int playerclass; // will be 1,2 or 3

int playersetup();
int playerclasspick();
int playerstats();   // defining all of our functions so I can right from top to bottom!
int beginGAME();
int fight1();
int fight2();
int fight3();
int fight4();
int fight5();

int main()
{
srand(time(0));

playersetup();

playerclasspick();

playerstats();

beginGAME();

return 0;
}


int playersetup()
{

cout << "Hello there dude! What is your name: ";

cin >> playername;

cout << "Let's begin our adventure, " << playername << "!" << endl;
Sleep(2);

cout << "Choose your class: " << endl;
cout << "1.Brawler" << endl;
cout << "2.Magician" << endl;
cout << "3.Gangster" << endl;

cin >> playerclass;

}

int playerclasspick()
{
    char rightclass; // y/n if they picked right class
    switch(playerclass){
        case 1: {
            playerclass = 1;
            cout << "You selected Brawler, are you sure? y/n: ";
            cin >> rightclass;
            if (rightclass == 'y')
            {
                cout << "Ok, congratulations, Brawler " << playername << "!" << endl;
                hp = 115;
                atk = 15;
                def = 20;
                mag = 0;
                }
            else {
                playerclasspick();
            }
        break;
        }


        case 2: {
            playerclass = 2;
            cout << "You selected Magician, are you sure? y/n: ";
            cin >> rightclass;
            if (rightclass == 'y'){
                cout << "Ok, congratulations, Magician " << playername << "!" << endl;
                hp = 90;
                atk = 9; // setting up the stats
                def = 12;
                mag = 25;
            }
            else {
                playerclasspick();
            }

        break; // exit our switch
        }


         case 3: {
            playerclass = 2;
            cout << "You selected Gangster, are you sure? y/n: ";
            cin >> rightclass;
            if (rightclass == 'y'){
                cout << "Ok, congratulations, gang-man " << playername << "!" << endl;
                hp = 150;
                atk = 20; // setting up the stats
                def = 8;
                mag = 0;
            }
            else {
                playerclasspick();
            }

        break; // exit our switch
        }

         default:
            playerclasspick();
    }


}

int playerstats()
{ // Will display all the player's stats
   cout << "Here are you stats, " << playername << endl;
   cout << "Attack: " << atk << endl;
   cout << "Defense: " << def << endl;
   cout << "Magic: " << mag << endl;
   cout <<  "Hit-Points: " << hp << endl;
}

int beginGAME()
{
    srand(time(0));
    int randomFIGHT = (rand()%5 + 1);

    switch (randomFIGHT){

    case 1:{
        fight1();
        break;
    }
    case 2:{
        fight2();
        break;
    }
    case 3:{
        fight3();
        break;
    }
    case 4:{
        fight4();
        break;
    }
    case 5:{
        fight5();
        break;
    }

    default:{
    cout << "Failed number???" << endl;
    }

}//end randomfight switch
} // end gamebegin


int fight1()
{
    cout << "You entered fight1!" << endl;
}

int fight2()
{
    cout << "You entered fight2!" << endl;
}

int fight3()
{
    cout << "You entered fight3!" << endl;
}

int fight4()
{
    cout << "You entered fight4!" << endl;
}

int fight5()
{
    cout << "You entered fight5!" << endl;
}
